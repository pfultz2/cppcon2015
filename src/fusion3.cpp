
#include <iostream>
#include <sstream>
#include <type_traits>
#include <boost/mpl/range_c.hpp>
#include <boost/fusion/container/map.hpp>
#include <boost/fusion/container/generation/make_map.hpp>
#include <boost/fusion/adapted/struct/define_assoc_struct.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/algorithm/transformation/zip.hpp>
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/fusion/sequence/intrinsic/at.hpp>
#include <boost/fusion/sequence/intrinsic/at_key.hpp>
#include <boost/fusion/sequence/intrinsic/size.hpp>
#include <boost/fusion/algorithm/transformation/transform.hpp>

using boost::mpl::range_c;
using namespace boost::fusion;
using boost::fusion::extension::struct_member_name;
using boost::fusion::extension::struct_assoc_key;

// sample(fusion_attributes)
// Primary key attribute
struct primary_key {};

// Attribute to specify max length
struct max_length_base {};
template<int N>
struct max_length : max_length_base
{
    static const int max_length_value = N;
};
// end-sample

// sample(fusion_has_attribute)
template<class Attribute, class Key>
constexpr auto has_attribute(const Key&)
{
    return std::is_base_of<Attribute, Key>();
}
// end-sample

namespace check_attributes {
// sample(fusion_check_attributes)
namespace fields
{
    struct name : primary_key, max_length<250>
    {};
    struct age
    {};
}

bool with_primary_key = has_attribute<primary_key>(fields::name());
bool without_primary_key = has_attribute<primary_key>(fields::age());
// end-sample

}

// sample(fusion_get_max_length)
#define REQUIRES(...) typename std::enable_if<(__VA_ARGS__), int>::type=0

template<class Key, REQUIRES(has_attribute<max_length_base>(Key()))>
int get_max_length(const Key&)
{
    return Key::max_length_value;
}

template<class Key, REQUIRES(!has_attribute<max_length_base>(Key()))>
int get_max_length(const Key&)
{
    return -1;
}
// end-sample

namespace person_map {

// sample(fusion_person_map)
namespace fields
{
    struct name
    {};
    struct age
    {};
}

typedef map<
    boost::fusion::pair<fields::name, std::string>
  , boost::fusion::pair<fields::age, int> >
person;
// end-sample

void exmap()
{
    // sample(fusion_make_person_map)
    person a_person = make_map<fields::name, fields::age>("Tom", 52);
    std::string person_name = at_key<fields::name>(a_person);
    int person_age = at_key<fields::age>(a_person);
    // end-sample
}

}

// sample(fusion_with_names_and_keys)
template<class Struct>
auto with_names_and_keys(const Struct& s)
{
    typedef range_c<int, 0, result_of::size<Struct>::type::value> range;
    static auto names = transform(range(), [](auto i) -> std::string
    {
        return struct_member_name<Struct, i>::call();
    });
    static auto keys = transform(range(), [](auto i)
    {
        return typename struct_assoc_key<Struct, i>::type();
    });
    return zip(s, names, keys);
}
// end-sample

// sample(fusion_get_value_name_key)
template<class T>
auto get_value(const T& x)
{
    return at_c<0>(x);
}

template<class T>
std::string get_name(const T& x)
{
    return at_c<1>(x);
}

template<class T>
auto get_key(const T& x)
{
    return at_c<2>(x);
}
// end-sample

template<class T>
using value_type = typename result_of::at_c<T, 0>::type;

template<class T>
using key_type = typename result_of::at_c<T, 2>::type;

// sample(fusion_person_fields)
namespace fields
{
    struct name : primary_key, max_length<250>
    {};
    struct age
    {};
}

BOOST_FUSION_DEFINE_ASSOC_STRUCT((), person,
    (std::string, name, fields::name)
    (int, age, fields::age)
);
// end-sample

template<class T>
void print_xml(const T& x)
{
    for_each(with_names_and_keys(x), [](const auto& x)
    {
        std::cout
            << '<' << get_name(x) << '>'
            << get_value(x)
            << "</" << get_name(x) << '>';
    });
}

// sample(fusion_create_column)
template<class T>
std::string create_column(const T& x)
{ 
    std::stringstream ss;
    ss << std::endl << "    " << get_name(x);
    if (std::is_convertible<decltype(get_value(x)), std::string>())
    {
        int max = get_max_length(get_key(x));
        if (max < 0) ss << " text ";
        else ss << " char(" << max << ")";
    }
    else ss << " " << typeid(get_value(x)).name();
    if (has_attribute<primary_key>(get_key(x))) ss << " primary key";
    return ss.str();
}
// end-sample

// sample(fusion_create_table)
template<class T>
std::string create_table(const T& x, const std::string& table_name)
{
    std::stringstream ss;
    ss << "create table " << table_name;
    char delim = '(';
    for_each(with_names_and_keys(x), [&](const auto& field)
    {
        ss << delim << create_column(field);
        delim = ',';
    });
    ss << ')';
    return ss.str();
}
// end-sample

// template<class T>
// auto get_primary_key(const T& x)
// {
//     typedef result::of::find_if<has_attribute<_, primary_key>>::type key;
//     return deref(find_if<has_attribute<_, primary_key>>(x)).second;
// }

void create_table_example()
{
    // sample(fusion_create_table_example)
    person p = { "Tom", 52 };
    std::cout << create_table(p, "person");
    // end-sample
    std::cout << std::endl;
}

int main()
{
    person p = { "Tom", 52 };
    // person p = make_map<fields::name, fields::age>("Tom", 52);
    print_xml(p);
    std::cout << std::endl;
    std::cout << create_table(p, "person");
    std::cout << std::endl;
    // std::cout << get_primary_key(p);
    // std::cout << std::endl;
    return 0;
}