
#include <iostream>
#include <sstream>
#include <type_traits>


// sample(visitor_simple_person)
struct person
{
    std::string name;
    int age;

    struct visit
    {
        template<class Self, class F>
        void operator()(Self&& self, F f) const
        {
            f("name", self.name);
            f("age", self.age);
        }
    };
};
// end-sample

template<class T, class F>
void visit_each(T&& x, F f)
{
    typedef typename std::remove_cv_t<std::remove_reference_t<T>>::visit visit;
    visit()(std::forward<T>(x), f);
}

// sample(visitor_simple_print_xml)
template<class T>
void print_xml(const T& x)
{
    visit_each(x, [](const std::string& name, const auto& var)
    {
        std::cout
            << '<' << name << '>'
            << var
            << "</" << name << '>';
    });
}
// end-sample

int main()
{
    // sample(visitor_simple_print_xml_example)
    person p = { "Tom", 52 };
    print_xml(p);
    // end-sample
    std::cout << std::endl;
}
