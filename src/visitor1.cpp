
#include <iostream>
#include <sstream>
#include <type_traits>
#include <typeinfo>

struct max_length
{
    int len;
    max_length(int x) : len(x)
    {}
};

struct primary_key
{};

struct person
{
    std::string name;
    int age;

    template<class F>
    void visit(F f) const
    {
        f("name", name, max_length(250), primary_key());
        f("age", age);
    }
};

template<class T>
void print_xml(const T& x)
{
    x.visit([](const std::string& name, const auto& var, const auto&... attributes)
    {
        std::cout
            << '<' << name << '>'
            << var
            << "</" << name << '>';
    });
}

template<class... Ts>
bool is_primary_key(const primary_key&, const Ts&...)
{
    return true;
}

bool is_primary_key()
{
    return false;
}

template<class T, class... Ts>
bool is_primary_key(const T&, const Ts&...xs)
{
    return is_primary_key(xs...);
}

template<class... Ts>
int get_max_length(const max_length& x, const Ts&...)
{
    return x.len;
}

int get_max_length()
{
    return -1;
}

template<class T, class... Ts>
int get_max_length(const T&, const Ts&...xs)
{
    return get_max_length(xs...);
}

template<class T, class... As>
std::string create_column(const std::string& name, const T& x, const As&... attributes)
{ 
    std::stringstream ss;
    ss << std::endl << "    " << name;
    if (std::is_convertible<decltype(x), std::string>())
    {
        int max = get_max_length(attributes...);
        if (max < 0) ss << " text ";
        else ss << " char(" << max << ")";
    }
    else ss << " " << typeid(x).name();
    if (is_primary_key(attributes...)) ss << " primary key";
    return ss.str();
}

template<class T>
std::string create_table(const T& x, const std::string& table_name)
{
    std::stringstream ss;
    ss << "create table " << table_name << "(";
    char delim = '(';
    x.visit([&](const std::string& name, const auto& var, const auto&... attributes)
    {
        ss << delim << create_column(name, var, attributes...);
        delim = ',';
    });
    ss << ')';
    return ss.str();
}

struct not_found {};

template<class Attribute>
struct attribute_finder
{
    template<class... Ts>
    static const Attribute& call(const Attribute& x, const Ts&...)
    {
        return x;
    }

    static not_found call()
    {
        return {};
    }

    template<class T, class... Ts>
    static auto call(const T&, const Ts&...xs)
    {
        return call(xs...);
    }
};

// template<class C, class Attribute>
// struct selector
// {
//     C& c;
//     selector(C& c) : c(c)
//     {}

//     template<class F>
//     void visit(F f) const
//     {
        
//     }
// };


int main()
{
    person p = { "Tom", 52 };
    print_xml(p);
    std::cout << std::endl;
    std::cout << create_table(p, "person");
    std::cout << std::endl;
    return 0;
}