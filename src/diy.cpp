
#include <iostream>
#include <sstream>
#include <type_traits>

// sample(diy_typed_expression)
#define REM(...) __VA_ARGS__
#define EAT(...)

// Strip off the type
#define STRIP(x) EAT x
// Show the type without parenthesis
#define PAIR(x) REM x
// end-sample

// sample(diy_each)
/* This counts the number of args */
#define NARGS_SEQ(_1,_2,_3,_4,_5,_6,_7,_8,N,...) N
#define NARGS(...) NARGS_SEQ(__VA_ARGS__, 8, 7, 6, 5, 4, 3, 2, 1)

/* This will let macros expand before concating them */
#define PRIMITIVE_CAT(x, y) x ## y
#define CAT(x, y) PRIMITIVE_CAT(x, y)

/* This will call a macro on each argument passed in */
#define EACH(macro, ...) CAT(EACH_, NARGS(__VA_ARGS__))(macro, __VA_ARGS__)
#define EACH_1(m, x0) m(x0, 0)
#define EACH_2(m, x0, x1) m(x0, 0) m(x1, 1)
#define EACH_3(m, x0, x1, x2) m(x0, 0) m(x1, 1) m(x2, 2)
#define EACH_4(m, x0, x1, x2, x3) m(x0, 0) m(x1, 1) m(x2, 2) m(x3, 3)
#define EACH_5(m, x0, x1, x2, x3, x4) m(x0, 0) m(x1, 1) m(x2, 2) m(x3, 3) m(x4, 4)
#define EACH_6(m, x0, x1, x2, x3, x4, x5) m(x0, 0) m(x1, 1) m(x2, 2) m(x3, 3) m(x4, 4) m(x5, 5)
#define EACH_7(m, x0, x1, x2, x3, x4, x5, x6) m(x0, 0) m(x1, 1) m(x2, 2) m(x3, 3) m(x4, 4) m(x5, 5) m(x6, 6)
#define EACH_8(m, x0, x1, x2, x3, x4, x5, x6, x7) m(x0, 0) m(x1, 1) m(x2, 2) m(x3, 3) m(x4, 4) m(x5, 5) m(x6, 6) m(x7, 7)
// end-sample

/* This will let macros expand before stringizing them */
#define STRINGIZE(x) PRIMITIVE_STRINGIZE(x)
#define PRIMITIVE_STRINGIZE(x) #x

// sample(diy_reflectable)
#define REFLECTABLE(...) \
static const int fields_n = NARGS(__VA_ARGS__); \
friend struct reflector; \
template<int N, class Self> \
struct field_data; \
EACH(REFLECT_EACH, __VA_ARGS__)
// end-sample

// sample(diy_reflect_each)
#define REFLECT_EACH(x, i) \
PAIR(x); \
template<class Self> \
struct field_data<i, Self> \
{ \
    Self & self; \
    field_data(Self & self) : self(self) {} \
    \
    auto& get() \
    { \
        return self.STRIP(x); \
    }\
    const auto& get() const \
    { \
        return self.STRIP(x); \
    }\
    const char * name() const \
    {\
        return STRINGIZE(STRIP(x)); \
    } \
};
// end-sample

// sample(diy_reflector)
struct reflector
{
    // Get field_data at index N
    template<int N, class T>
    static auto get_field_data_at(T& x)
    {
        return typename T::template field_data<N, T>(x);
    }

    // Iterate through each field
    template<class F, class T, int N=T::fields_n>
    struct each_field_data
    {
        static void call(F f, T& x)
        {
            each_field_data<F, T, N-1>::call(f, x);
            f(get_field_data_at<N-1>(x));
        }
    };

    template<class F, class T>
    struct each_field_data<F, T, 0>
    {
        static void call(F, T&) {}
    };
};
// end-sample

// sample(diy_visit_each)
template<class C, class Visitor>
void visit_each(C & c, Visitor v)
{
    reflector::each_field_data<Visitor, C>::call(v, c);
}
// end-sample

// sample(diy_print_xml)
template<class T>
void print_xml(const T& x)
{
    visit_each(x, [](auto data)
    {
        std::cout
            << '<' << data.name() << '>'
            << data.get()
            << "</" << data.name() << '>';
    });
}
// end-sample

// sample(diy_reflectable_person)
struct person
{
    REFLECTABLE
    (
        (std::string) name,
        (int) age
    )
};
// end-sample

int main()
{
    person p = { "Tom", 52 };
    print_xml(p);
    std::cout << std::endl;
    return 0;
}
