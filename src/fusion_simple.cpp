#include <iostream>
#include <sstream>
#include <type_traits>
#include <boost/mpl/eval_if.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/identity.hpp>
#include <boost/fusion/adapted/std_tuple.hpp>
#include <boost/fusion/adapted/struct/adapt_assoc_struct.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/algorithm/query/find_if.hpp>
#include <boost/fusion/container/generation/make_map.hpp>
#include <boost/fusion/iterator/deref.hpp>

using boost::mpl::eval_if;
using boost::mpl::_;
using boost::mpl::identity;
using boost::fusion::at_c;
using boost::fusion::for_each;
using boost::fusion::find_if;
using boost::fusion::make_map;
using boost::fusion::map;
using boost::fusion::deref;

// sample(simple_print_xml)
template<class T>
void print_xml(const T& t)
{
    for_each(t, [](const auto& x)
    {
        std::cout
            << '<' << typeid(x).name() << '>'
            << x
            << "</" << typeid(x).name() << '>';
    });
}
// end-sample



int main()
{
    // sample(fusion_simple)
    auto stuff = std::make_tuple(1, 'x', "howdy");
    int i = at_c<0>(stuff);
    char ch = at_c<1>(stuff);
    std::string s = at_c<2>(stuff);
    // end-sample

    print_xml(stuff);
}
