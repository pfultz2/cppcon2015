
#include <iostream>
#include <sstream>
#include <type_traits>

// sample(diy2_typed_expression)
#define REM(...) __VA_ARGS__
#define EAT(...)

// Strip off the type
#define STRIP(x) EAT x
// Show the type without parenthesis
#define PAIR(x) REM x
// end-sample

// sample(diy2_each)
/* This counts the number of args */
#define NARGS_SEQ(_1,_2,_3,_4,_5,_6,_7,_8,N,...) N
#define NARGS(...) NARGS_SEQ(__VA_ARGS__, 8, 7, 6, 5, 4, 3, 2, 1)

/* This will let macros expand before concating them */
#define PRIMITIVE_CAT(x, y) x ## y
#define CAT(x, y) PRIMITIVE_CAT(x, y)

/* This will call a macro on each argument passed in */
#define EACH(macro, ...) CAT(EACH_, NARGS(__VA_ARGS__))(macro, __VA_ARGS__)
#define EACH_1(m, x0) m(x0)
#define EACH_2(m, x0, x1) m(x0) m(x1)
#define EACH_3(m, x0, x1, x2) m(x0) m(x1) m(x2)
#define EACH_4(m, x0, x1, x2, x3) m(x0) m(x1) m(x2) m(x3)
#define EACH_5(m, x0, x1, x2, x3, x4) m(x0) m(x1) m(x2) m(x3) m(x4)
#define EACH_6(m, x0, x1, x2, x3, x4, x5) m(x0) m(x1) m(x2) m(x3) m(x4) m(x5)
#define EACH_7(m, x0, x1, x2, x3, x4, x5, x6) m(x0) m(x1) m(x2) m(x3) m(x4) m(x5) m(x6)
#define EACH_8(m, x0, x1, x2, x3, x4, x5, x6, x7) m(x0) m(x1) m(x2) m(x3) m(x4) m(x5) m(x6) m(x7)
// end-sample

// sample(diy2_enum)
/* Prepend a comma */
#define PRIMITIVE_COMMA(m) , m
#define COMMA(m) PRIMITIVE_COMMA EAT() (m)

/* Remove the first argument and return its tail */
#define PRIMITIVE_TAIL(x, ...) __VA_ARGS__
#define TAIL(...) PRIMITIVE_TAIL(__VA_ARGS__)

/* Enumerate the macro on each argument */
#define ENUM(macro, ...) TAIL(EACH(COMMA(macro), __VA_ARGS__))

/* This will let macros expand before stringizing them */
#define STRINGIZE(x) PRIMITIVE_STRINGIZE(x)
#define PRIMITIVE_STRINGIZE(x) #x
// end-sample

// sample(diy2_reflectable)
#define REFLECTABLE(...) \
EACH(MEMBER_EACH, __VA_ARGS__) \
struct unpack \
{ \
    template<class T, class F> \
    static void apply(T&& self, F&& f) \
    { \
        f(ENUM(UNPACK_EACH, __VA_ARGS__)); \
    } \
};
// end-sample

// sample(diy2_reflect_each)
#define MEMBER_EACH(x) PAIR(x);

#define UNPACK_EACH(x) \
std::pair<const char*, decltype((self.STRIP(x)))>(STRINGIZE(STRIP(x)), self.STRIP(x))
// end-sample


// sample(diy2_visit_each)
template<class C, class F>
void for_each(C&& c, F f)
{
    using unpack = typename std::remove_cv_t<std::remove_reference_t<C>>::unpack;
    unpack::apply(c, [&](auto&&... xs)
    {
        (void)std::initializer_list<int>{
            (f(std::forward<decltype(xs)>(xs)), 0)...
        };
    });
}
// end-sample

// sample(diy2_print_xml)
template<class T>
void print_xml(const T& x)
{
    for_each(x, [](auto data)
    {
        std::cout
            << '<' << data.first << '>'
            << data.second
            << "</" << data.first << '>';
    });
}
// end-sample

// sample(diy2_reflectable_person)
struct person
{
    REFLECTABLE
    (
        (std::string) name,
        (int) age
    )
};
// end-sample

int main()
{
    person p = { "Tom", 52 };
    print_xml(p);
    std::cout << std::endl;
    return 0;
}
