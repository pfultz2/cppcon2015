
#include <iostream>
#include <sstream>
#include <type_traits>
#include <boost/mpl/range_c.hpp>
#include <boost/fusion/adapted/struct/define_struct.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/algorithm/transformation/zip.hpp>
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/fusion/sequence/intrinsic/at.hpp>
#include <boost/fusion/sequence/intrinsic/size.hpp>
#include <boost/fusion/algorithm/transformation/transform.hpp>

using boost::mpl::range_c;
using namespace boost::fusion;
using boost::fusion::extension::struct_member_name;

// sample(fusion_with_names)
template<class Struct>
auto with_names(const Struct& s)
{
    using range = range_c<int, 0, (result_of::size<Struct>())>;
    static auto names = transform(range(), [](auto i) -> std::string
    {
        return struct_member_name<Struct, i>::call();
    });
    return zip(s, names);
}
// end-sample

// sample(fusion_get_value_and_name)
template<class T>
auto get_value(const T& x)
{
    return at_c<0>(x);
}

template<class T>
std::string get_name(const T& x)
{
    return at_c<1>(x);
}
// end-sample

#if BOOST_FUSION_ADAPT_STRUCT
struct person
{
    std::string name;
    int age;
};

BOOST_FUSION_ADAPT_STRUCT(person,
    (std::string, name)
    (int, age)
);
#else
// sample(fusion_define_person_struct)
BOOST_FUSION_DEFINE_STRUCT((), person,
    (std::string, name)
    (int, age)
);
// end-sample

#endif

// sample(fusion_print_xml_with_names)
template<class T>
void print_xml(const T& x)
{
    for_each(with_names(x), [](const auto& x)
    {
        std::cout
            << '<' << get_name(x) << '>'
            << get_value(x)
            << "</" << get_name(x) << '>';
    });
}
// end-sample

void get_member_names()
{
    // sample(fusion_get_member_names)
    std::string name = struct_member_name<person, 0>::call();
    std::string age = struct_member_name<person, 1>::call();
    // end-sample

    std::cout << name << std::endl;
    std::cout << age << std::endl;
}

int main()
{
    // person p = make_map<fields::name, fields::age>("Tom", 52);
    // sample(fusion_print_adapted_person_xml)
    person p = { "Tom", 52 };
    std::string name = at_c<0>(p);
    int age = at_c<1>(p);
    
    print_xml(p);
    // end-sample
    std::cout << std::endl;

    get_member_names();
    return 0;
}