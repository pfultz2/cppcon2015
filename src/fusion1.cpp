
#include <iostream>
#include <sstream>
#include <type_traits>
#include <boost/mpl/eval_if.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/identity.hpp>
#include <boost/fusion/adapted/struct/adapt_assoc_struct.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/algorithm/query/find_if.hpp>
#include <boost/fusion/container/generation/make_map.hpp>
#include <boost/fusion/iterator/deref.hpp>

using boost::mpl::eval_if;
using boost::mpl::_;
using boost::mpl::identity;
using boost::fusion::for_each;
using boost::fusion::find_if;
using boost::fusion::make_map;
using boost::fusion::map;
using boost::fusion::deref;

// Primary key attribute
struct primary_key {};

// Attribute to specify max length
struct max_length_base {};
template<int N>
struct max_length : max_length_base
{
    static const int max_length_value = N;
};

template<class T>
struct holder
{
    using type = void;
};

template<class Pair, class Attribute, class=void>
struct has_attribute
: std::false_type
{};

template<class Pair, class Attribute>
struct has_attribute<Pair, Attribute, 
    typename holder<typename Pair::first_type>::type>
: std::is_base_of<Attribute, typename Pair::first_type>::type
{};

// template<class Pair>
// using is_primary_key = 
// has_attribute<Pair, primary_key>;

// template<class Pair>
// struct get_max_length_helper
// {
//     using type = std::integral_constant<int, Pair::first_type::max_length_value>;
// };

// template<class Pair>
// struct get_max_length
// : eval_if<has_attribute<Pair, max_length_base>,
//     get_max_length_helper<Pair>,
//     std::integral_constant<int, -1>
// >::type
// {};

#define REQUIRES(...) std::enable_if_t<(__VA_ARGS__), int> = 0

template<class Pair, REQUIRES(has_attribute<Pair, max_length_base>())>
bool get_max_length(const Pair&)
{
    return Pair::first_type::max_length_value;
}

template<class Pair, REQUIRES(!has_attribute<Pair, max_length_base>())>
bool get_max_length(const Pair&)
{
    return -1;
}


namespace fields
{
    struct name : primary_key, max_length<250>
    {
        static const char * field_name()
        {
            return "name";
        }
    };
    struct age
    {
        static const char * field_name()
        {
            return "age";
        }
    };
}

// struct person
// {
//     std::string name;
//     int age;
// };

// BOOST_FUSION_ADAPT_ASSOC_STRUCT(person,
//     (std::string, name, fields::name)
//     (int, age, fields::age)
// )

typedef map<
    boost::fusion::pair<fields::name, std::string>
  , boost::fusion::pair<fields::age, int> >
person;

template<class Pair>
const char * get_name(Pair)
{
    return Pair::first_type::field_name();
}

template<class T>
void print_xml(const T& x)
{
    for_each(x, [](const auto& p)
    {
        std::cout
            << '<' << get_name(p) << '>'
            << p.second
            << "</" << get_name(p) << '>';
    });
}

template<class Pair>
std::string create_column(const Pair& p)
{ 
    std::stringstream ss;
    ss << std::endl << "    " << get_name(p);
    if (std::is_same<typename Pair::second_type, std::string>())
    {
        int max = get_max_length(p);
        if (max < 0) ss << " text ";
        else ss << " char(" << max << ")";
    }
    else ss << " " << typeid(p.second).name();
    if (has_attribute<Pair, primary_key>()) ss << " primary key";
    return ss.str();
}

template<class T>
std::string create_table(const T& x, const std::string& table_name)
{
    std::stringstream ss;
    ss << "create table " << table_name;
    char delim = '(';
    for_each(x, [&](const auto& p)
    {
        ss << delim << create_column(p);
        delim = ',';
    });
    ss << ')';
    return ss.str();
}

template<class T>
auto get_primary_key(const T& x)
{
    return deref(find_if<has_attribute<_, primary_key>>(x)).second;
}


int main()
{
    // person p = { "Tom", 52 };
    person p = make_map<fields::name, fields::age>("Tom", 52);
    print_xml(p);
    std::cout << std::endl;
    std::cout << create_table(p, "person");
    std::cout << std::endl;
    std::cout << get_primary_key(p);
    std::cout << std::endl;
    return 0;
}