
#include <iostream>
#include <sstream>
#include <type_traits>
#include <typeinfo>

// sample(visitor_attributes)
struct max_length
{
    int len;
    max_length(int x) : len(x)
    {}
};

struct primary_key
{};
// end-sample

// sample(visitor_person_with_attributes)
struct person
{
    std::string name;
    int age;

    struct visit
    {
        template<class Self, class F>
        void operator()(Self&& self, F f) const
        {
            f("name", self.name, max_length(250), primary_key());
            f("age", self.age);
        }
    };
};
// end-sample

// sample(visitor_visit_each)
template<class T, class F>
void visit_each(T&& x, F f)
{
    using visit = typename std::remove_cv_t<std::remove_reference_t<T>>::visit;
    visit()(std::forward<T>(x), f);
}
// end-sample

// sample(visitor_print_xml)
template<class T>
void print_xml(const T& x)
{
    visit_each(x, [](const std::string& name, const auto& var, const auto&... attributes)
    {
        std::cout
            << '<' << name << '>'
            << var
            << "</" << name << '>';
    });
}
// end-sample

// sample(visitor_is_primary_key)
template<class... Ts>
bool is_primary_key(const primary_key&, const Ts&...)
{
    return true;
}

bool is_primary_key()
{
    return false;
}

template<class T, class... Ts>
bool is_primary_key(const T&, const Ts&...xs)
{
    return is_primary_key(xs...);
}
// end-sample

// sample(visitor_get_max_length)
template<class... Ts>
int get_max_length(const max_length& x, const Ts&...)
{
    return x.len;
}

int get_max_length()
{
    return -1;
}

template<class T, class... Ts>
int get_max_length(const T&, const Ts&...xs)
{
    return get_max_length(xs...);
}
// end-sample

// sample(visitor_create_column)
template<class T, class... As>
std::string create_column(const std::string& name, const T& x, const As&... attributes)
{ 
    std::stringstream ss;
    ss << std::endl << "    " << name;
    if (std::is_convertible<decltype(x), std::string>())
    {
        int max = get_max_length(attributes...);
        if (max < 0) ss << " text ";
        else ss << " char(" << max << ")";
    }
    else ss << " " << typeid(x).name();
    if (is_primary_key(attributes...)) ss << " primary key";
    return ss.str();
}
// end-sample

// sample(visitor_create_table)
template<class T>
std::string create_table(const T& x, const std::string& table_name)
{
    std::stringstream ss;
    ss << "create table " << table_name << "(";
    char delim = '(';
    visit_each(x, [&](const std::string& name, const auto& var, const auto&... attributes)
    {
        ss << delim << create_column(name, var, attributes...);
        delim = ',';
    });
    ss << ')';
    return ss.str();
}
// end-sample

// sample(visitor_attribute_finder)
struct not_found {};

template<class Attribute>
struct attribute_finder
{
    template<class... Ts>
    static const Attribute& call(const Attribute& x, const Ts&...)
    {
        return x;
    }

    static not_found call()
    {
        return {};
    }

    template<class T, class... Ts>
    static auto call(const T&, const Ts&...xs)
    {
        return call(xs...);
    }
};
// end-sample

// template<class Attribute>
// struct attribute_selector
// {
//     template<class F, class... Ts>
//     static const Attribute& call(const Attribute& x, const Ts&...)
//     {
//         return f;
//     }

//     template<class F>
//     static auto call(F f)
//     {
//         // Do Nothing
//         return [](auto&&...) {};
//     }

//     template<class F, class T, class... Ts>
//     static auto call(F f, const T&, const Ts&...xs)
//     {
//         return call(xs...);
//     }
// };


// template<class Attribute, class... Ts>
// const Attribute& find_attribute(const Attribute& x, const Ts&...)
// {
//     return x;
// }

// template<class Attribute>
// not_found find_attribute()
// {
//     return {};
// }

// template<class Attribute, class T, class... Ts>
// auto find_attribute(const T&, const Ts&...xs)
// {
//     return find_attribute<Attribute>(xs...);
// }

// sample(visitor_if_found)
template<class Attribute, class F>
auto if_found(const Attribute& attribute, F f)
{
    return f;
}

template<class F>
auto if_found(not_found, F)
{
    // Do nothing
    return [](auto&&...) {};
}
// end-sample

// sample(visitor_visit_select)
template<class Attribute, class T, class F>
void visit_select(T&& x, F f)
{
    visit_each(x, [f](const std::string& name, auto&& var, const auto&... attributes)
    {
        const auto& attribute = attribute_finder<Attribute>::call(attributes...);
        if_found(attribute, f)(name, var, attribute);
    });
}
// end-sample

// sample(visitor_visit_primary_key)
template<class T, class F>
void visit_primary_key(const T& x, F f)
{
    return visit_select<primary_key>(x, f);
}
// end-sample

void visit_select_example()
{
    // sample(visitor_visit_select_example)
    person p = { "Tom", 52 };
    visit_select<primary_key>(p, [](const std::string& name, 
                                    const auto& x, primary_key)
    {
        std::cout << name << ": " << x;
    });
    // end-sample
}


int main()
{
    // sample(visitor_create_table_example)
    person p = { "Tom", 52 };
    std::cout << create_table(p, "person");
    // end-sample
    std::cout << std::endl;
    print_xml(p);
    std::cout << std::endl;
    // sample(visitor_visit_primary_key_example)
    visit_primary_key(p, [](const std::string& name, const auto& x, primary_key)
    {
        std::cout << name << ": " << x;
    });
    // end-sample
    std::cout << std::endl;
    visit_select_example();
    std::cout << std::endl;
    return 0;
}